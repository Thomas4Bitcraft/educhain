<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => ['auth','role:student|admin|university|company'], 'prefix' => 'home'], function () {

    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/shared-with-me', 'HomeController@sharedWithMe')->name('shared-with-me');
    Route::get('/file/{id}', 'HomeController@viewFile')->name('view-file');
});

Route::group(['middleware' => ['auth','role:admin|university'], 'prefix' => 'home'], function () {
    Route::get('/file-upload-for-student', 'HomeController@fileUploadForStudent')->name('file-upload-for-student');

    Route::post('/file-upload-for-student', 'HomeController@uploadFileForStudent')->name('file-upload-for-student-post');
});

Route::group(['middleware' => ['auth','role:admin|university|student'], 'prefix' => 'home'], function () {
    Route::get('/file-upload', 'HomeController@fileUpload')->name('file-upload');
    Route::get('/my-files', 'HomeController@myFiles')->name('my-files');

    Route::post('/file-upload', 'HomeController@uploadFile')->name('file-upload-post');
    Route::post('/file-share', 'HomeController@shareFile')->name('file-share-post');
    Route::post('/file-revoke', 'HomeController@revokeFile')->name('file-revoke');
});

Route::group(['middleware' => ['auth','role:admin'], 'prefix' => 'home'], function () {
    Route::get('/users', 'HomeController@users')->name('users');
    Route::get('/user/{id}', 'HomeController@user')->name('user');

    Route::post('/edit-role/{id}', 'HomeController@editRole')->name('edit-role');
});

