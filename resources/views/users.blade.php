@extends('layouts.dashboard')

@section('content')
    <h3 class="uk-card-title">User</h3>
    <p>All Users of Educhain</p>
    <p style="color:red">{{session('error') ?? ''}}</p>
    <table class="uk-table uk-table-striped">
        <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Role</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
                @if(!$user->hasRole('admin'))
                    <tr>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->getRoleNames()[0]}}</td>
                        <td><a class="uk-button uk-button-primary" type="button" href="/home/user/{{$user->id}}">Edit Role</a></td>
                    </tr>
                @endif
            @endforeach
        </tbody>
    </table>
@endsection
