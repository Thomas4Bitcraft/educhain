@extends('layouts.dashboard')

@section('content')
    <h3 class="uk-card-title">User {{$user->name}}</h3>
    <p>Email: {{$user->email}}</p>
    <form method="POST" action="/home/edit-role/{{$user->id}}">
        @csrf
        <fieldset class="uk-fieldset">
                <div class="uk-margin">
                    <label class="uk-form-label" for="form-stacked-select">Role</label>
                    <div class="uk-form-controls uk-form-width-medium">
                        <select name="role" class="uk-select" id="form-stacked-select">
                            @foreach ($roles as $role)
                                <option @if($user->hasRole($role)) selected @endif>{{$role}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            <div class="uk-margin" uk-margin>
                <button class="uk-button uk-button-default">Save</button>
            </div>
        </fieldset>
    </form>
@endsection
