@extends('layouts.dashboard')

@section('content')
    <h3 class="uk-card-title">WELCOME {{auth()->user()->name}}</h3>
    @if(auth()->user()->hasRole('student'))
        <p>Welcome to your personal dashboard where you can manage all your diplomas in one place!</p>
    @elseif(auth()->user()->hasRole('university'))
        <p>Welcome to your personal dashboard where you can manage all your personal files and upload diplomas for students.</p>
    @elseif(auth()->user()->hasRole('company'))
        <p>Welcome to your personal dashboard where you can view all diplomas of your applicants in one place!</p>
    @elseif(auth()->user()->hasRole('admin'))
        <p>Admin Dashboard</p>
    @endif
@endsection
