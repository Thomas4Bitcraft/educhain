@extends('layouts.dashboard')

@section('content')
    <h3 class="uk-card-title">Diplomas shared with me</h3>
    <p>Here is an overview of all diplomas that are shared with you</p>

    <table class="uk-table uk-table-striped">
        <thead>
            <tr>
                <th>Name</th>
                <th>Shared At</th>
                <th>Owner</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($files as $file)
                <tr>
                    <td>{{$file["name"]}}</td>
                    <td>{{$file["shared_at"]}}</td>
                    <td>{{@App\User::where('email',$file["owner"])->get()->first()->name}}</td>
                    <td><a class="uk-button uk-button-primary" target="_blank" type="button" href="/home/file/{{$file["hash"]}}">View</a></td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <div id="modal" uk-modal>
        <div class="uk-modal-dialog uk-modal-body">
            <button class="uk-modal-close-default" type="button" uk-close></button>
            <h2 class="uk-modal-title">Share File</h2>
            <p>Enter receiver and share your file</p>
            <form action="/home/file-share" method="POST">
                @csrf
                <fieldset class="uk-fieldset">
                    <div class="uk-margin">
                        <input class="uk-input uk-form-width-medium" name="with" type="text" placeholder="Receiver Email Address">
                        <input id="hash" hidden name="hash">
                        <input id="hash" hidden name="name">
                        <button class="uk-button uk-button-primary" type="submit">Share</button>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>

    <script>
        function setFile(hash, name) {
            $('#hash')[0].value = hash;
            $('#name')[0].value = name;
        }



    </script>
@endsection
