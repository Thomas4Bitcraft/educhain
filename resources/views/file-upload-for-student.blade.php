@extends('layouts.dashboard')

@section('content')
    <h3 class="uk-card-title">File Upload For Student</h3>
    <p>Upload a file for a student</p>
    <p style="color:red">{{session('error') ?? ''}}</p>
    <form method="POST" action="{{route('file-upload-for-student-post')}}" enctype="multipart/form-data">
        @csrf
        <fieldset class="uk-fieldset">
            <div class="uk-margin">
                <input class="uk-input uk-form-width-large" name="student" type="text" placeholder="Email of student">
            </div>
            <div class="uk-margin" uk-margin>
                <div uk-form-custom="target: true">
                    <input type="file" name="userfile">
                    <input class="uk-input uk-form-width-medium" type="text" placeholder="Select file" disabled>
                </div>
                <button class="uk-button uk-button-primary">Submit</button>
            </div>
        </fieldset>
    </form>
@endsection
