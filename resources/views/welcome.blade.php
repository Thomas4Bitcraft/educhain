@extends('layouts.app')

@section('content')
<section id="content" class="uk-section uk-section-default">
<div class="uk-container">
    <div class="uk-section uk-section-small uk-padding-remove-top">
        <ul class="uk-subnav uk-subnav-pill uk-flex uk-flex-center" data-uk-switcher="connect: .uk-switcher; animation: uk-animation-fade">
            <li><a class="uk-border-pill" href="#">Collect</a></li>
            <li><a class="uk-border-pill" href="#">Manage</a></li>
            <li><a class="uk-border-pill" href="#">Share</a></li>
        </ul>
    </div>

    <ul class="uk-switcher uk-margin">
        <li>
            <div class="uk-grid uk-child-width-1-2@l uk-flex-middle" data-uk-grid data-uk-scrollspy="target: > div; cls: uk-animation-slide-left-medium">
                <div>
                    <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="img/marketing-1.svg" alt="" data-uk-img>
                </div>
                <div data-uk-scrollspy-class="uk-animation-slide-right-medium">
                    <h6 class="uk-text-primary">Collect</h6>
                    <h2 class="uk-margin-small-top">Upload your diplomas or get them from your university</h2>
                    <p class="subtitle-text">
                        You can easily upload your diplomas via the application or get them directly from your university. All your files are stored encrypted in IPFS.
                    </p>
                </div>
            </div>
        </li>
        <li>
            <div class="uk-grid uk-child-width-1-2@l uk-flex-middle" data-uk-grid data-uk-scrollspy="target: > div; cls: uk-animation-slide-left-medium">
                <div>
                    <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="img/marketing-8.svg" alt="" data-uk-img>
                </div>
                <div data-uk-scrollspy-class="uk-animation-slide-right-medium">
                    <h6 class="uk-text-primary">Manage</h6>
                    <h2 class="uk-margin-small-top">Manage from one place</h2>
                    <p class="subtitle-text">
                        You can manage all your diplomas on your personal dashboard. Wherever you are.
                    </p>
                </div>
            </div>
        </li>
        <li>
            <div class="uk-grid uk-child-width-1-2@l uk-flex-middle" data-uk-grid data-uk-scrollspy="target: > div; cls: uk-animation-slide-left-medium">
                <div>
                    <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="img/marketing-9.svg" alt="" data-uk-img>
                </div>
                <div data-uk-scrollspy-class="uk-animation-slide-right-medium">
                    <h6 class="uk-text-primary">Share</h6>
                    <h2 class="uk-margin-small-top">Share them with other users</h2>
                    <p class="subtitle-text">
                        You want to apply for a job or university? Then share relevant diplomas with other people secure via the Hyperledger Blockchain.
                    </p>
                </div>
            </div>
        </li>
    </ul>
</div>
</section>
<section class="uk-section uk-section-secondary uk-section-large">
<div class="uk-container">
    <div class="uk-grid uk-child-width-1-2@l uk-flex-middle">
        <div>
            <h6>MANAGE YOU DIPLOMAS</h6>
            <h2 class="uk-margin-small-top uk-h1">Manage all your diplomas from one place only.</h2>
            <p class="subtitle-text">
                With the educhain you can safely manage and share your diplomas as long as you want and with whom you want.
            </p>
            <a href="/register" class="uk-button uk-button-primary uk-border-pill" data-uk-icon="arrow-right">REGISTER</a>
        </div>
        <div data-uk-scrollspy="cls: uk-animation-fade">
            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="img/marketing-2.svg" data-uk-img alt="Image">
        </div>
    </div>
</div>
</section>
<section class="uk-cover-container overlay-wrap">
<img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-srcset="https://picsum.photos/640/650/?image=770 640w,
https://picsum.photos/960/650/?image=770 960w,
https://picsum.photos/1200/650/?image=770 1200w,
https://picsum.photos/2000/650/?image=770 2000w"
data-sizes="100vw"
data-src="https://picsum.photos/1200/650/?image=770" alt="" data-uk-cover data-uk-img
>
<div class="uk-container uk-position-z-index uk-position-relative uk-section uk-section-xlarge uk-light">
    <div class="uk-grid uk-flex-right">
        <div class="uk-width-2-5@m" data-uk-parallax="y: 50,-50; easing: 0; media:@l">
            <h6>DISCOVER THE TECHNOLOGIES</h6>
            <div class="uk-position-relative uk-visible-toggle uk-light" data-uk-slider="easing: cubic-bezier(.16,.75,.47,1)">
                <ul class="uk-slider-items uk-child-width-1-1">
                    <li>
                        <div data-uk-slider-parallax="opacity: 0.2,1,0.2">
                            <h4 class="uk-margin-small-top"><a target="_blank" style="text-decoration: underline;" href="https://ipfs.io/">IPFS:</a><br>InterPlanetary File System (IPFS) is a protocol and network designed to create a content-addressable, peer-to-peer method of storing and sharing hypermedia in a distributed file system. Similar to a torrent, IPFS allows users to not only receive but host content.</h4>
                        </div>
                    </li>
                    <li>
                        <div data-uk-slider-parallax="opacity: 0.2,1,0.2">
                            <h4 class="uk-margin-small-top"><a target="_blank" style="text-decoration: underline;" href="https://www.hyperledger.org/projects/fabric">Hyperledger Fabric:</a><br>A blockchain framework implementation and one of the Hyperledger projects hosted by The Linux Foundation. Intended as a foundation for developing applications or solutions with a modular architecture, Hyperledger Fabric allows components, such as consensus and membership services, to be plug-and-play.</h4>
                        </div>
                    </li>
                    <li>
                        <div data-uk-slider-parallax="opacity: 0.2,1,0.2">
                            <h4 class="uk-margin-small-top"><a target="_blank" style="text-decoration: underline;" href="https://laravel.com/">Laravel:</a><br>A free, open-source PHP web framework, created by Taylor Otwell and intended for the development of web applications following the model–view–controller (MVC) architectural pattern and based on Symfony.</h4>
                        </div>
                    </li>
                </ul>
                <ul class="uk-slider-nav uk-dotnav uk-margin-top"><li></li></ul>

            </div>
        </div>

    </div>
</div>
</section>
<!-- BOTTOM -->
<section class="uk-section uk-section-default">

<div class="uk-container uk-container-xsmall uk-text-center uk-section uk-padding-remove-top">
    <h5 class="uk-text-primary">FEATURES</h5>
    <h2 class="uk-margin-remove uk-h1">Manage your files</h2>
</div>
<div class="uk-container">
    <div class="uk-grid uk-grid-large uk-child-width-1-3@m" data-uk-grid data-uk-scrollspy="target: > div; delay: 150; cls: uk-animation-slide-bottom-medium">
        <div class="uk-text-center">
            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="img/marketing-3.svg" data-uk-img alt="Image">
            <h4 class="uk-margin-small-bottom uk-margin-top uk-margin-remove-adjacent">Upload files</h4>
            <p>Save your files audit-proof in the blockchain</p>
        </div>
        <div class="uk-text-center">
            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="img/marketing-5.svg" data-uk-img alt="Image">
            <h4 class="uk-margin-small-bottom uk-margin-top uk-margin-remove-adjacent">Share files</h4>
            <p>Give other users access to files with the touch of a button</p>
        </div>
        <div class="uk-text-center">
            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="img/marketing-4.svg" data-uk-img alt="Image">
            <h4 class="uk-margin-small-bottom uk-margin-top uk-margin-remove-adjacent">Full Control</h4>
            <p>Delete a user's access to files</p>
        </div>
    </div>
</div>
</section>
<!-- BOTTOM -->
@stop
