<!DOCTYPE html>
<html lang="en" class="uk-height-1-1">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Login</title>
		<link rel="icon" href="img/favicon.ico">
		<!-- CSS FILES -->
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.1.5/css/uikit.min.css">
	</head>
	<body class="uk-height-1-1 uk-cover-container uk-background-secondary">

		<img data-srcset="https://picsum.photos/640/700/?image=1044 640w,
		             https://picsum.photos/960/700/?image=1044 960w,
		             https://picsum.photos/1200/900/?image=1044 1200w,
		             https://picsum.photos/2000/1000/?image=1044 2000w"
		     sizes="100vw"
		     data-src="https://picsum.photos/1200/900/?image=1044" alt="" data-uk-cover data-uk-img
		>

		<div class="uk-position-cover uk-overlay-primary"></div>

		<div class="uk-flex uk-flex-center uk-flex-middle uk-height-viewport uk-light uk-position-relative uk-position-z-index">
            @yield('content')
		</div>

		<!-- JS FILES -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.1.5/js/uikit.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.1.5/js/uikit-icons.min.js"></script>
	</body>
</html>
