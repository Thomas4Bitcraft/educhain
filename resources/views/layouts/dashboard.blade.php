<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
        <meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Educhain</title>
		<link rel="icon" href="img/favicon.ico">
		<!-- CSS FILES -->
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.1.5/css/uikit.min.css">
        <link rel="stylesheet" type="text/css" href="css/marketing.css">
        <script
            src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous">
        </script>
</head>
<body>
    <div class="nav uk-sticky uk-background-secondary uk-box-shadow-medium uk-sticky-below uk-sticky-fixed">
        <!-- NAV -->
        <div class="nav" style="border-bottom: 1px solid gray;">
            <div class="uk-container">
                <nav class="uk-navbar uk-navbar-container uk-navbar-transparent" data-uk-navbar>
                    <div class="uk-navbar-left">
                        <div class="uk-navbar-item uk-padding-remove-horizontal">
                            <a href="/" class="uk-logo" title="Logo" href="">Educhain</a>
                        </div>
                    </div>
                    <div class="uk-navbar-right">
                        <ul class="uk-navbar-nav uk-visible@s">
                            @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                            @else
                                <li>
                                    <a href="#" data-uk-icon="chevron-down">{{ Auth::user()->name }}</a>
                                    <div class="uk-navbar-dropdown">
                                        <ul class="uk-nav uk-navbar-dropdown-nav">
                                            <li><a href="/home">Dashboard</a></li>
                                            <li><a class="dropdown-item" href="{{ route('logout') }}"
                                                onclick="event.preventDefault();
                                                                document.getElementById('logout-form').submit();">
                                                    {{ __('Logout') }}
                                                </a>

                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    @csrf
                                                </form>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            @endguest
                        </ul>
                        <a class="uk-navbar-toggle uk-navbar-item uk-hidden@s" data-uk-toggle data-uk-navbar-toggle-icon href="#offcanvas-nav"></a>
                    </div>
                </nav>
            </div>
        </div>
        <!-- /NAV -->
    </div>
    <div style="margin-top: 50px;">
        <section id="content" class="uk-section uk-section-default">
            <div class="uk-container">
                <div uk-grid>
                    <div class="uk-width-1-4@m">
                        @include('partials.sidebar')
                    </div>
                    <div class="uk-width-expand@m">
                        <div class="uk-card uk-card-default uk-card-body">
                            @yield('content')
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- JS FILES -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.1.5/js/uikit.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.1.5/js/uikit-icons.min.js"></script>
</body>
</html>
