@extends('layouts.dashboard')

@section('content')
    <h3 class="uk-card-title">My Diplomas</h3>
    <p>Here is an overview of all your diplomas which can be shared with other users.</p>
    <p style="color:red">{{session('error') ?? ''}}</p>
    <table class="uk-table uk-table-striped">
        <thead>
            <tr>
                <th>Name</th>
                <th>Uploaded</th>
                <th>Shared With</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($files as $file)
                <tr>
                    <td>{{$file["name"]}}</td>
                    <td>{{$file["uploaded"]}}</td>
                    <td>
                        @if(is_array($file["shared_with"]))
                            @foreach ($file["shared_with"] as $shared)
                            <form action="/home/file-revoke" method="POST">
                                @csrf
                                <input type="text" name="user" hidden value="{{$shared}}">
                                <input type="text" name="file" hidden value="{{$file["hash"]}}">
                                <span>{{@App\User::where('email',$shared)->first()->name}}</span> <button style="font-size: 16px;padding-bottom: 3px;" class="uk-button uk-button-link" type="submit">Revoke</button><br>
                            </form>
                            @endforeach
                        @endif
                    </td>
                    <td><a target="_blank" class="uk-button uk-button-primary" type="button" href="/home/file/{{$file["hash"]}}">View</a></td>
                    <td><button class="uk-button uk-button-primary" onclick="setFile('{{$file['hash']}}','{{$file['name']}}')" type="button" uk-toggle="target: #modal">Share</button></td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <div id="modal" uk-modal>
        <div class="uk-modal-dialog uk-modal-body">
            <button class="uk-modal-close-default" type="button" uk-close></button>
            <h2 class="uk-modal-title">Share File</h2>
            <p>Enter receiver and share your file</p>
            <form action="/home/file-share" method="POST">
                @csrf
                <fieldset class="uk-fieldset">
                    <div class="uk-margin">
                        <input class="uk-input uk-form-width-medium" name="with" type="email" required placeholder="Receiver Email Address">
                        <input id="hash" hidden name="hash">
                        <input id="filename" hidden name="filename">
                        <button class="uk-button uk-button-primary" type="submit">Share</button>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>

    <script>
        function setFile(hash, name) {
            $('#hash')[0].value = hash;
            $('#filename')[0].value = name;
        }



    </script>
@endsection
