<div class="uk-card uk-card-default uk-card-body">
    <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
        <li class="uk-active"><a><h4>Menu</h4></a></li>
        <li class="uk-parent">
            <a href="#"><span class="uk-margin-small-right" uk-icon="icon: thumbnails"></span> Diplomas</a>
            <ul class="uk-nav-sub">
                @if(auth()->user()->hasRole('student') ||
                    auth()->user()->hasRole('admin') ||
                    auth()->user()->hasRole('university'))
                    <li><a href="/home/my-files"><span class="uk-margin-small-right" uk-icon="icon: album"></span>My Files</a></li>
                    <li><a href="/home/file-upload"><span class="uk-margin-small-right" uk-icon="icon: push"></span>Upload File</a></li>
                @endif
                @if(auth()->user()->hasRole('student') ||
                    auth()->user()->hasRole('admin') ||
                    auth()->user()->hasRole('university') ||
                    auth()->user()->hasRole('company'))
                    <li><a href="/home/shared-with-me"><span class="uk-margin-small-right" uk-icon="icon: social"></span>Shared with me</a></li>
                @endif
                @if(auth()->user()->hasRole('university'))
                    <li><a href="/home/file-upload-for-student"><span class="uk-margin-small-right" uk-icon="icon: social"></span>Upload File For Student</a></li>
                @endif
                @if(auth()->user()->hasRole('admin'))
                    <li><a href="/home/users"><span class="uk-margin-small-right" uk-icon="icon: user"></span>All User</a></li>
                @endif
            </ul>
        </li>
    </ul>
</div>
