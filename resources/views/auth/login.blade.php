@extends('layouts.login-template')

@section('content')
<div class="uk-width-medium uk-padding-small" uk-scrollspy="cls: uk-animation-fade">
    <div class="uk-text-center uk-margin">
        <h2>Educhain Login</h2>
    </div>
    <form method="POST" action="{{ route('login') }}">
        @csrf
        <fieldset class="uk-fieldset">
            <div class="uk-margin">
                <div class="uk-inline uk-width-1-1">
                    <span class="uk-form-icon uk-form-icon-flip" data-uk-icon="icon: user"></span>
                    <input id="email" name="email" value="{{ old('email') }}" autofocus class="uk-input uk-border-pill" required placeholder="Email" type="email">
                </div>
                @error('email')
                    <span>
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="uk-margin">
                <div class="uk-inline uk-width-1-1">
                    <span class="uk-form-icon uk-form-icon-flip" data-uk-icon="icon: lock"></span>
                    <input id="password" type="password" class="uk-input uk-border-pill" name="password" required autocomplete="current-password" placeholder="Password">
                </div>
                @error('password')
                    <span>
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="uk-margin">
                <label><input class="uk-checkbox" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}> Keep me logged in</label>
            </div>
            <div class="uk-margin">
                <button type="submit" class="uk-button uk-button-primary uk-border-pill uk-width-1-1">LOG IN</button>
            </div>
        </fieldset>
    </form>
    <!--
        <div>
            <div class="uk-text-center">
                @if (Route::has('password.request'))
                    <a href="{{ route('password.request') }}" class="uk-link-reset uk-text-small" data-uk-toggle="target: #recover;animation: uk-animation-slide-top-small">Forgot your password?</a>
                @endif
            </div>
        </div>
    -->
</div>
@endsection
