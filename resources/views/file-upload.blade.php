@extends('layouts.dashboard')

@section('content')
    <h3 class="uk-card-title">File Upload</h3>
    <p>Upload a file for sharing</p>
    <p style="color:red">{{session('error') ?? ''}}</p>
    <form method="POST" action="{{route('file-upload-post')}}" enctype="multipart/form-data">
        @csrf
        <div class="uk-margin" uk-margin>
            <div uk-form-custom="target: true">
                <input type="file" name="userfile">
                <input class="uk-input uk-form-width-medium" type="text" placeholder="Select file" disabled>
            </div>
            <button class="uk-button uk-button-primary">Submit</button>
        </div>

    </form>
@endsection
