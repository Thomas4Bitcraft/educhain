<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use Spatie\Permission\Models\Role;
use Symfony\Component\Process\Process;

class GetFiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:files';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        User::first()->assignRole('student');
        dd();
        dd(exec('docker exec -it fabric-cli bash -c "cd /tmp; bash scripts/invoke_query.sh get a"'));
        $crawlProcess = new Process(['docker', 'exec', 'fabric-cli', 'bash', '-c', '"cd /tmp; bash scripts/invoke_query.sh get a"']);
        $crawlProcess->setTimeout(3600000000);
        $crawlProcess->run(function ($type, $buffer) {
            if (Process::ERR === $type) {
                echo 'ERR > ' . $buffer;
            } else {
                echo 'OUT > ' . $buffer;
            }
        });
    }
}
