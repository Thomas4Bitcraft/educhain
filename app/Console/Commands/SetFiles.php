<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SetFiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'set:files {user} {files}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = $this->argument('user');
        $files = $this->argument('files');
        $data = exec("docker exec -it fabric-cli bash -c \"cd /tmp; bash scripts/invoke_query.sh set $user $files\"");
        echo $data;
    }
}
