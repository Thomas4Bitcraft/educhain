<?php

namespace App\Http\Controllers;

use App\File;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Cloutier\PhpIpfsApi\IPFS;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Crypt;
use Symfony\Component\Process\Process;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use Symfony\Component\Process\Exception\ProcessFailedException;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function fileUpload()
    {
        return view('file-upload');
    }

    public function myFiles()
    {
        //$files = $this->getFilesForUser(Auth::user()->email);
        $json_dec = Crypt::decrypt(Auth::user()->json);
        $files = json_decode($json_dec, true);
        dd($files);
        $myFiles = [];
        foreach($files as $file) {
            if($file["owner"] == Auth::user()->id) {
                $myFiles[] = $file;
            }
        }

        return view('my-files', [
            'files' => $myFiles
        ]);
    }

    public function viewFile($id) {
        $file = $this->getFile($id);
        $base64_image = base64_encode($file);
        header('Content-type: application/pdf');
        header('filename="diploma.pdf"');
        echo base64_decode($base64_image);
    }

    public function uploadFile(Request $request)
    {
        $file = $request->file('userfile');

        if($file->getClientMimeType() != "application/pdf") {
            return back()->with('error', 'Only PDF Files are allowed');
        }

        $filename = $file->getClientOriginalName();
        $enc_file = Crypt::encrypt($file->get());
        $ipfs = new IPFS("localhost", "8080", "5001");
        $hash = $ipfs->add($enc_file);
        $enc_hash = Crypt::encryptString($hash);

        $file_json = $this->getFilesForUser(Auth::user()->email);

        array_push($file_json, [
            "hash" => $enc_hash,
            "owner" => Auth::user()->id,
            "name" => $filename,
            "uploaded" => Carbon::now()->toDateString(),
            "shared_with" => "[]"
        ]);

        // save to blockchain
        $this->saveFilesForUsers(Auth::user()->email, $file_json);

        return redirect('/home/my-files');
    }

    public function shareFile(Request $request) {
        $with = User::where('email', $request->with)->first();

        // check if user exists
        if(!isset($with)) {
            return Redirect::back()
            ->with('error','User not found!');
        }

        // check if user is self
        if($with->email == Auth::user()->email) {
            return Redirect::back()
            ->with('error','You cant share files with yourself ;)');
        }


        $file_json = $this->getFilesForUser($with->email);
        $file_json[] = [
            "hash" => $request->hash,
            "owner" => Auth::user()->email,
            "name" => $request->filename,
            "shared_at" => Carbon::now()->toDateString()
        ];

        // save to blockchain
        $this->saveFilesForUsers($with->email, $file_json);

        // get files
        $user_files = $this->getFilesForUser(Auth::user()->email);

        foreach($user_files as $index => $file) {
            if($file["hash"] == $request->hash) {

                if(!is_array($file["shared_with"])) {
                    $shared_with = json_decode($file["shared_with"], true);
                } else {
                    $shared_with = $file["shared_with"];
                }

                $shared_with[] = $with->email;
                $user_files[$index]["shared_with"] = $shared_with;
            }
        }
        // save to blockchain
        $this->saveFilesForUsers(Auth::user()->email, $user_files);

        return back();
    }

    public function revokeFile(Request $request) {
        $file_hash = $request->file;
        $user = User::where('email', $request->user)->first();

        $user_files = $this->getFilesForUser(Auth::user()->email);
        $shared_user_files = $this->getFilesForUser($user->email);

        // shared user files
        foreach($shared_user_files as $index => $file) {
            if($file['hash'] == $file_hash && $file['owner'] == Auth::user()->email) {
                unset($shared_user_files[$index]);
            }
        }
        // save to blockchain
        $this->saveFilesForUsers($user->email, $shared_user_files);

        // authenticated user files
        foreach($user_files as $index => $file) {
            if($file['hash'] == $file_hash && is_array($file['shared_with'])) {
                foreach($file['shared_with'] as $index_user => $shared) {
                    if($user->email == $shared) {
                        unset($user_files[$index]['shared_with'][$index_user]);
                    }
                }
            }
        }
        // save to blockchain
        $this->saveFilesForUsers(Auth::user()->email, $user_files);

        return back();
    }

    public function sharedWithMe() {

        $files = $this->getFilesForUser(Auth::user()->email);
        $shared = [];
        foreach($files as $file) {
            if($file["owner"] != Auth::user()->id && isset($file["shared_at"])) {
                $shared[] = $file;
            }
        }

        return view('shared-with-me', [
            'files' => $shared
        ]);
    }

    public function getFile($hash)
    {
        $ipfs = new IPFS("localhost", "8080", "5001");
        $hash = Crypt::decryptString($hash);

        // remove IPFS hash value in file
        $enc_file = preg_replace('/[ \t].*/', '', $ipfs->cat($hash));
        $file = Crypt::decrypt($enc_file);
        return $file;
    }

    public function fileUploadForStudent() {
        return view('file-upload-for-student');
    }

    public function uploadFileForStudent(Request $request) {
        $file = $request->file('userfile');
        $user = User::where('email', $request->student)->first();

        if($file->getClientMimeType() != "application/pdf") {
            return back()->with('error', 'Only PDF Files are allowed');
        }

        if(!isset($user)) {
            return back()->with('error', 'Student does not exists in our system');
        }

        if(!$user->hasRole('student')) {
            return back()->with('error', 'User is not a student');
        }

        $filename = $file->getClientOriginalName();
        $enc_file = Crypt::encrypt($file->get());
        $ipfs = new IPFS("localhost", "8080", "5001");
        $hash = $ipfs->add($enc_file);
        $enc_hash = Crypt::encryptString($hash);

        $file_json = $this->getFilesForUser($user->email);
        array_push($file_json, [
            "hash" => $enc_hash,
            "owner" => $user->id,
            "name" => $filename,
            "uploaded" => Carbon::now()->toDateString(),
            "shared_with" => "[]"
        ]);

        // save to blockchain
        $this->saveFilesForUsers($user->email, $file_json);
        return back();
    }

    public function users() {
        $users = User::all();
        return view('users', ['users' => $users]);
    }

    public function user($id) {
        $user = User::findOrFail($id);
        $roles = ['student', 'company', 'university'];
        return view('user', ['user' => $user, 'roles' => $roles]);
    }

    public function editRole(Request $request, $id) {
        $user = User::findOrFail($id);
        $user->syncRoles([$request->role]);
        $user->save();
        return redirect('/home/users');
    }

    // Get Data from Blockchain
    public function getFilesForUser($email) {
        $process = new Process("sudo docker exec fabric-cli bash -c \"cd /tmp; bash scripts/invoke_query.sh get $email\"");
        $process->setWorkingDirectory('/');
        $process->run();

        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
        $json_enc = explode("\n", $process->getOutput())[3];
        $json_dec = Crypt::decrypt($json_enc);
        return json_decode($json_dec, true);
    }

    // Set data on Blockchain
    public function saveFilesForUsers($email, $files) {
        $json_enc = Crypt::encrypt(json_encode($files));
        $process = new Process("sudo docker exec fabric-cli bash -c \"cd /tmp; bash scripts/invoke_query.sh set $email $json_enc\"");
        $process->setWorkingDirectory('/');
        $process->run();

        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        // save in db for test cases
        $user = User::where('email', $email)->first();
        $user->json = $json_enc;
        $user->save();
    }
}
