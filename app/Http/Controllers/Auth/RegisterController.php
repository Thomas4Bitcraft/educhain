<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\Process\Process;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Symfony\Component\Process\Exception\ProcessFailedException;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $empty = 'eyJpdiI6IlBvWFwvUFNlSWNGdFJOcWNkcEVaRGxRPT0iLCJ2YWx1ZSI6IjcwRXpQZzlUNXpoN0hwN09HNkF4QlE9PSIsIm1hYyI6ImMwYWNiZmI2OTk1N2Y3ODk2NzE0YWU2ZjkwNzA5ZTYyZmFmNzgzZDQ3OTYyOTc2YmFmOTk1ODZkZTQ1ZjUzMTcifQ==';
        $email = $data['email'];
        $process = new Process("sudo docker exec fabric-cli bash -c \"cd /tmp; bash scripts/invoke_query.sh set $email $empty\"");
        $process->setWorkingDirectory('/');
        $process->run();

        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'json' => 'eyJpdiI6IlBvWFwvUFNlSWNGdFJOcWNkcEVaRGxRPT0iLCJ2YWx1ZSI6IjcwRXpQZzlUNXpoN0hwN09HNkF4QlE9PSIsIm1hYyI6ImMwYWNiZmI2OTk1N2Y3ODk2NzE0YWU2ZjkwNzA5ZTYyZmFmNzgzZDQ3OTYyOTc2YmFmOTk1ODZkZTQ1ZjUzMTcifQ=='
        ]);

        $user->assignRole('student');
        return $user;
    }
}
